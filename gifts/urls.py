from django.conf.urls import patterns, include, url
from django.contrib import admin


urlpatterns = patterns(
    '',

    url(r'^admin/', include(admin.site.urls)),

    # namespace нужен для деления ссылок на группы, указывающие к чему именно
    # они относятся. Например, может быть несколько приложений, в которых
    # встречается ссылка с одинаковым названием, но благодаря namespace-ам
    # эти ссылки можно отличить друг от друга.
    url(r'', include('gifts.gifts.urls', namespace='gifts')),
)
