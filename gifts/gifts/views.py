from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.template.loader import render_to_string

from gifts.gifts.models import Holiday


def holiday_view(request, key):
    """Просмотр праздника"""
    holiday = get_object_or_404(Holiday.objects_open, key=key)
    return render(request, 'gifts/holiday.html', {'holiday': holiday})


def reserve_view(request, key, gift_id):
    """Бронирование подарка и снятие брони
    :param request: HttpRequest Запрос
    :param key: секретный ключ праздника
    :param gift_id: идентификатор подарка
    """
    if request.method != 'POST':
        # HTTP-код 405 означает, что метод не разрешен
        # В нашем случае мы принимает только POST-запросы на изменение состояния
        # брони подарка/
        # http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.6
        return HttpResponse(status=405)

    # Ищем запршенный проздник
    holiday = get_object_or_404(Holiday, key=key, opened=True)
    # Ищем подарок
    gift = get_object_or_404(holiday.gift_set, pk=gift_id)

    data = request.POST
    action = data.get('action', 'reserve')
    if action != 'reserve':
        # HTTP-код 400 Bad Request говорит, что сервер не смог понять запрос
        # В нашем случае - неверное действие
        # http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.1
        return HttpResponse(status=400)

    # Валидация данных может быть сделана с помощью форм, но в данном примере
    # выполняется "ручная" проверка без форм.

    valid = True  # Полагаем изначально, что данные корректные
    msg = ''  # Сообщение, которое будем показывать пользователю

    name = data.get('name', '').strip()  # Убираем пробелы из начала и конца
    email = data.get('email', '')
    try:
        validate_email(email)
    except ValidationError:
        valid = False
        msg = 'Введите корректный e-mail'

    if not valid:
        return JsonResponse({'msg': msg})

    # Данные верные, можно выполнять соответствующее действие

    # Бронирование подарка
    gift.reserve_email = email
    gift.reserve_name = name
    gift.save()

    # Если есть сообщение, значит, что-то пошло не так
    if msg:
        return JsonResponse({'msg': msg})

    # Сообщения нет, всё в порядке.
    # Рендерим новое состояние подарка и отдаем клиенту.
    html = render_to_string('gifts/include/gift.html',
                            {'gift': gift, 'holiday': holiday})
    return JsonResponse({'msg': '', 'html': html})


def discard_view(request, key, gift_id):
    """Бронирование подарка и снятие брони
    :param request: HttpRequest Запрос
    :param key: секретный ключ праздника
    :param gift_id: идентификатор подарка
    """
    if request.method != 'POST':
        # HTTP-код 405 означает, что метод не разрешен
        # В нашем случае мы принимает только POST-запросы на изменение состояния
        # брони подарка/
        # http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.6
        return HttpResponse(status=405)

    # Ищем запршенный проздник
    holiday = get_object_or_404(Holiday, key=key, opened=True)
    # Ищем подарок
    gift = get_object_or_404(holiday.gift_set, pk=gift_id)

    data = request.POST
    action = data.get('action', 'discard')
    if action != 'discard':
        # HTTP-код 400 Bad Request говорит, что сервер не смог понять запрос
        # В нашем случае - неверное действие
        # http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.1
        return HttpResponse(status=400)

    # Валидация данных может быть сделана с помощью форм, но в данном примере
    # выполняется "ручная" проверка без форм.

    valid = True  # Полагаем изначально, что данные корректные
    msg = ''  # Сообщение, которое будем показывать пользователю

    # Для снятия брони имя не нужно, только email
    email = data.get('email', '')
    try:
        validate_email(email)
    except ValidationError:
        valid = False
        msg = 'Введите корректный e-mail'

    if not valid:
        return JsonResponse({'msg': msg})

    # Данные верные, можно выполнять соответствующее действие

    # Снятие брони
    gift.reserve_email = ''
    gift.reserve_name = ''
    gift.save()

    # Если есть сообщение, значит, что-то пошло не так
    if msg:
        return JsonResponse({'msg': msg})

    # Сообщения нет, всё в порядке.
    # Рендерим новое состояние подарка и отдаем клиенту.
    html = render_to_string('gifts/include/gift.html',
                            {'gift': gift, 'holiday': holiday})
    return JsonResponse({'msg': '', 'html': html})
